-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 23 Feb 2019 pada 11.41
-- Versi Server: 10.1.9-MariaDB
-- PHP Version: 7.0.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko_pizza`
--
CREATE DATABASE IF NOT EXISTS `toko_pizza` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `toko_pizza`;

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_karyawan`
--

CREATE TABLE `master_karyawan` (
  `nik` varchar(5) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `master_karyawan`
--

INSERT INTO `master_karyawan` (`nik`, `nama`, `alamat`, `telp`, `tempat_lahir`, `tanggal_lahir`, `flag`) VALUES
('BG01', 'kojo', 'jl.sungai bambu no 17', '08234567953', 'Jakarta', '1959-01-01', 1),
('BR03', 'Muhrin Hajrin', 'Jl. Enim Raya No. 112', '089621650780', 'Yogyakarta', '1993-06-14', 1),
('BR04', 'Murni Wati Hajrin', 'Jl.Kampung Baru No, 120', '082383908234', 'Kampung Baru', '1999-08-24', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_menu`
--

CREATE TABLE `master_menu` (
  `kode_menu` varchar(5) NOT NULL,
  `nama_menu` varchar(100) NOT NULL,
  `harga` float NOT NULL,
  `keterangan` text NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `master_menu`
--

INSERT INTO `master_menu` (`kode_menu`, `nama_menu`, `harga`, `keterangan`, `flag`) VALUES
('RG01', 'Vaggie Garden Pizza', 40000, 'jagung,jamur,keju Mozarella, Paprika Merah, Paprika Hijau, Bawang Bombay', 1),
('RG02', 'Tuna Melt', 50000, 'Irisan Daging ikan tuna, Butiran Jagung, Saus mayonnaise', 1),
('RG03', 'Meat Lover', 70000, 'Irisan Daging sapi cincang, Burger Sapi, Sosis ayam', 1),
('RG04', 'Pepperoni', 50000, 'Pizza dengan topping pepperoni sapi, saus tomat, dan keju mozzarella', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi_pemesanan`
--

CREATE TABLE `transaksi_pemesanan` (
  `id_pemesanan` int(11) NOT NULL,
  `nik` varchar(5) NOT NULL,
  `tgl_pemesanan` date NOT NULL,
  `nama_pelanggan` varchar(100) NOT NULL,
  `kode_menu` varchar(5) NOT NULL,
  `qty` int(11) NOT NULL,
  `total_harga` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi_pemesanan`
--

INSERT INTO `transaksi_pemesanan` (`id_pemesanan`, `nik`, `tgl_pemesanan`, `nama_pelanggan`, `kode_menu`, `qty`, `total_harga`) VALUES
(5, 'BR03', '2019-02-23', 'murni', 'RG03', 7, 490000),
(6, 'BG01', '2019-02-23', 'Muhrin', 'RG02', 3, 150000),
(7, 'BG01', '2019-02-23', 'Putri', 'RG04', 12, 600000),
(8, 'BR04', '2019-02-23', 'Iing ', 'RG02', 7, 350000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `master_karyawan`
--
ALTER TABLE `master_karyawan`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `master_menu`
--
ALTER TABLE `master_menu`
  ADD PRIMARY KEY (`kode_menu`);

--
-- Indexes for table `transaksi_pemesanan`
--
ALTER TABLE `transaksi_pemesanan`
  ADD PRIMARY KEY (`id_pemesanan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `transaksi_pemesanan`
--
ALTER TABLE `transaksi_pemesanan`
  MODIFY `id_pemesanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
