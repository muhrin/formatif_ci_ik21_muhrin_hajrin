
<body bgcolor="#666666">
<table width="40%" height="70" border="0" align="center">
<tr>
 
  <td colspan="5" align="center" bgcolor="#FF0000"><b>EDIT KARYAWAN</td></b>
 
 
  
  </tr>
  <?php
foreach ($detail_karyawan as $data) {
	$nik  = $data->nik;
	$nama_lengkap  = $data->nama;
	$alamat  = $data->alamat;
	$telp  = $data->telp;
	$tempat_lahir  = $data->tempat_lahir;
	$tgl_lahir  = $data->tanggal_lahir;
	

}

$thn_pisah = substr($tgl_lahir, 0, 4);
$bln_pisah = substr($tgl_lahir, 5, 2);
$tgl_pisah = substr($tgl_lahir, 8, 2);
?>
<form action="<?=base_url()?>karyawan/editkaryawan/<?= $nik; ?>" method="POST">
<table width="40%" border="0" cellspacing="0" cellpadding="5" bgcolor="#00FFFF" align="center">


    
  <tr>
    <br />  
    <td>Nik</td>
    <td>:</td>
    <td>
      <input type="text" name="nik" id="nik" maxlength="10" value ="<?=$nik;?>" readonly ></td>
  </tr>
  <tr>
    <td>Nama Karyawan</td>
    <td>:</td>
    <td>
      <input type="text" name="nama_lengkap" id="nama_lengkap" maxlength="50" value ="<?=$nama_lengkap;?>" ></td>
  </tr>
  
   <tr>
    <td>Alamat</td>
    <td>:</td>
    <td><textarea name="alamat" id="alamat" cols="45" rows="5" ><?=$alamat;?></textarea></td>
  </tr>
  
  <tr>
    <td>Telepon</td>
    <td>:</td>
    <td><input type="text" name="telp" id="telp" value ="<?=$telp;?>" ></td>
    </td>
  </tr>
  
  <tr>
    <td>Tempat Lahir</td>
    <td>:</td>
    <td> <input type="text" name="tempat_lahir" id="tempat_lahir" maxlength="50" value ="<?=$tempat_lahir;?>"  ></td>
    </td>
  </tr>
 
  <tr>
    <td>Tanggal Lahir</td>
    <td>:</td>
    <td><select name="tgl" id="tgl">
    <?php
	for($tgl=1;$tgl<=31;$tgl++){
		$select_tgl = ($tgl == $tgl_pisah) ? 'selected' : '';
		?>
        <option value="<?=$tgl;?>" <?=$select_tgl; ?>><?=$tgl;?></option>
        <?php
	}
	?>
    </select>
      <select name="bln" id="bln">
       <?php
	   $bln_n = array('januari','februari','maret','april','mei','juni','juli','agustus','september','oktober','november','desember');
	for($bln=0;$bln<12;$bln++){
		$select_bln = ($bln == $bln_pisah) ? 'selected' : '';
		?>
        <option value="<?=$bln+1;?>" <?=$select_bln; ?>>
		<?=$bln_n[$bln];?>
        </option>
        <?php
	}
	?>
      </select>
      <select name="thn" id="thn">
       <?php
	for($thn=date('Y')-60;$thn<=date('Y')-15;$thn++){
		$select_thn = ($thn == $thn_pisah) ? 'selected' : '';
		?>
        <option value="<?=$thn;?>" <?=$select_thn; ?>><?=$thn;?></option>
        <?php
	}
	?>
      </select>
      </td>
  </tr>
  

  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><input type="submit" name="simpan" id="simpan" value="simpan">
      <input type="submit" name="batal" id="batal" value="reset">
      <br></br>
      <a href="<?=base_url();?>karyawan/listkaryawan"><input type="button" name="kembali ke menu sebelumnya" id="kembali ke menu sebelumnya" value="kembali ke menu sebelumnya"></a></td>
  </tr>
</table>
</table>
</form>
</body>
